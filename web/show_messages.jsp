<%@ page import="java.util.List" %>
<%@ page import="uz.pdp.model.Messages" %>
<%@ page import="uz.pdp.model.Users" %>
<%@ page import="uz.pdp.dbConnection.DBConnection" %>
<%@ page import="java.sql.SQLException" %><%--
  Created by IntelliJ IDEA.
  User: hp
  Date: 18.09.2022
  Time: 14:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Show Messages</title>
</head>
<body>
<br>
<center><h2 style="color: green">Your Messages</h2></center>
<%
    HttpSession session1 = request.getSession();
    List<Messages> messages = (List<Messages>) session.getAttribute("messages");
    Users current_user = (Users) session1.getAttribute("user");
    Boolean isNew = (Boolean) session1.getAttribute("isNew");
%>
<br>
<div align="center">
    <table style="border: black solid 1px">
        <tr style="border: black solid 1px">
            <th style="border: black solid 1px">fromUser</th>
            <th style="border: black solid 1px">Time</th>
            <th style="border: black solid 1px">text</th>
            <th style="border: black solid 1px">action</th>
        </tr>
        <%
            for (Messages message : messages) {
                Users fromUser;
                try {
                    fromUser = DBConnection.getUserById(message.getFromUser());
                } catch (SQLException | ClassNotFoundException e) {
                    throw new RuntimeException(e);
                }
                out.println("<tr style=\"border: black solid 1px\">");
                out.println("<td style=\"border: black solid 1px\">"+ fromUser.getUserName());
                out.println("</td>");
                out.println("<td style=\"border: black solid 1px\">"+message.getDate());
                out.println("</td>");
                out.println("</td>");
                out.println("<td style=\"border: black solid 1px\">"+message.getText());
                out.println("</td>");
                out.println("<td style=\"border: black solid 1px\">");
                out.println("<button><a href=\"/delete?id="+current_user.getId()+"&message_id="+message.getId()+"\">Delete</a></button>");
                out.println("<button><a href=\"/writeMessage?to_user_id="+fromUser.getId()+"&from_user_id="+current_user.getId()+"\">Write</a></button>");
                out.println("</td>");
                out.println("</tr>");
                if (isNew){
                    try {
                        DBConnection.setRead(message);
                    } catch (SQLException | ClassNotFoundException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        %>
    </table>
</div>
<br>
<center>
    <h3>
        <button >
            <a href="/user_cabinet.jsp" style="text-decoration: none; color: red"><-back</a>
        </button>
    </h3>
</center>
</body>
</html>
