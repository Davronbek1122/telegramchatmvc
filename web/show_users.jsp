<%--
  Created by IntelliJ IDEA.
  User: Alisher
  Date: 9/13/2022
  Time: 11:10 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Title</title>
</head>
<body>
<br>
<center>
    <h2 style="color: chocolate">
        List Of Users
    </h2>
</center>
<br>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.List" %>
<%@ page import="uz.pdp.model.Users" %>
<%
    HttpSession session1 = request.getSession();
    List<Users> usersList = (List<Users>) session1.getAttribute("users");
    Users current_user = (Users) session1.getAttribute("user");
%>
<br>
<br>
<%--<center><h2><a href="/saveOrEdit">Add NEW BOOK</a></h2></center>--%>
<br>
<br>
<div align="center">
    <table style="border: black solid 1px">
        <tr style="border: black solid 1px">
            <th style="border: black solid 1px">id</th>
            <th style="border: black solid 1px">userName</th>
            <th style="border: black solid 1px">phoneNumber</th>
            <th style="border: black solid 1px">action</th>
        </tr>
        <%
                for (Users user : usersList) {
                    out.println("<tr style=\"border: black solid 1px\">");
                    out.println("<td style=\"border: black solid 1px\">"+user.getId());
                    out.println("</td>");
                    out.println("<td style=\"border: black solid 1px\">"+user.getUserName());
                    out.println("</td>");
                    out.println("</td>");
                    out.println("<td style=\"border: black solid 1px\">"+user.getPhoneNumber());
                    out.println("</td>");
                    out.println("<td style=\"border: black solid 1px\">");
                    if (user.isActive()){
                        out.println("<button><a href=\"/activeOrBlock?id="+user.getId()+"&activate="+1+"\">Block</a></button>");
                    }else {
                        out.println("<button><a href=\"/activeOrBlock?id="+user.getId()+"&activate="+0+"\">Active</a></button>");
                    }
                    out.println("</td>");
                    out.println("</tr>");
                }
        %>
    </table>
</div>
<br>
<center>
    <h3>
        <button >
            <a href="/index.html" style="text-decoration: none; color: red">LogOut</a>
        </button>
    </h3>
</center>
</body>
</html>
