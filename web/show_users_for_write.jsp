<%@ page import="uz.pdp.model.Users" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: hp
  Date: 18.09.2022
  Time: 18:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Write message</title>
</head>
<body>
<br>
<center>
    <h3 style="color: chocolate">
        Please choose user to write
    </h3>
</center>
<br>
<%
    HttpSession session1 = request.getSession();
    List<Users> usersList = (List<Users>) session1.getAttribute("users");
    Users current_user = (Users) session1.getAttribute("user");
%>

<div align="center">
    <table style="border: black solid 1px">
        <tr style="border: black solid 1px">
            <th style="border: black solid 1px">userName</th>
            <th style="border: black solid 1px">action</th>
        </tr>
        <%
            for (Users user : usersList) {
                out.println("<tr style=\"border: black solid 1px\">");
                out.println("<td style=\"border: black solid 1px\">"+user.getUserName());
                out.println("</td>");
                out.println("<td style=\"border: black solid 1px\">");
                out.println("<button><a href=\"/writeMessage?to_user_id="+user.getId()+"&from_user_id="+current_user.getId()+"\">Write</a></button>");
                out.println("</td>");
                out.println("</tr>");
            }
        %>
    </table>
</div>
<center>
    <h3>
        <a href="user_cabinet.jsp"  style="text-decoration: none;">Orqaga</a>
    </h3>
</center>
</body>
</html>
