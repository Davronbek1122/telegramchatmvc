package uz.pdp;

import lombok.SneakyThrows;
import uz.pdp.dbConnection.DBConnection;
import uz.pdp.model.Users;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.List;

public class BlockOrActive extends HttpServlet {
    @SneakyThrows
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Users user = (Users) session.getAttribute("user");
        Integer id = Integer.valueOf(request.getParameter("id"));
        int nowActive= Integer.parseInt(request.getParameter("activate"));
        boolean activate=false;
        if (nowActive==0){
            activate=true;

        }
        DBConnection.setActiveOrBlock(id,activate);
        List<Users> usersList = DBConnection.getUsers(2, null);
        session.setAttribute("users",usersList);
        response.sendRedirect("/show_users.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
