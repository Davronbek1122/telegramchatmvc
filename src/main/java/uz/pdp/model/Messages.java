package uz.pdp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Messages {
    private Integer id;
    private Integer fromUser;
    private Integer toUser;
    private String text;
    private Date date;
    private boolean isRead;
    private boolean isDeleted;
}
