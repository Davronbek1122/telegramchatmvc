package uz.pdp.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Users {
    private Integer id;
    private Integer role_id;
    private String userName;
    private String password;
    private String phoneNumber;
    private boolean isActive;
}
