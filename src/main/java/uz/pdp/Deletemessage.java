package uz.pdp;

import lombok.SneakyThrows;
import uz.pdp.dbConnection.DBConnection;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;

public class Deletemessage extends HttpServlet {
    @SneakyThrows
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer id = Integer.valueOf(request.getParameter("id"));
        Integer message_id = Integer.valueOf(request.getParameter("message_id"));
        DBConnection.deleteMessage(message_id);
        response.sendRedirect("/user_cabinet.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
