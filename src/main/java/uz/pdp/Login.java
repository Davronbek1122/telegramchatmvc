package uz.pdp;

import lombok.SneakyThrows;
import uz.pdp.dbConnection.DBConnection;
import uz.pdp.model.Users;
import uz.pdp.utilss.Methods;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class Login extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/login.jsp");
        dispatcher.forward(request,response);
    }

    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        HttpSession session = request.getSession();
        Users user = DBConnection.getUserByPassUserName(username, password);
        session.setAttribute("user",user);
        Boolean toAccess = Methods.checkToAccess(request, user.getRole_id());
        if (user != null || user.getPassword() != null){
            if (toAccess != null && toAccess && user.getRole_id()==1){
                List<Users> usersList = DBConnection.getUsers(2, user.getId());
                session.setAttribute("users",usersList);
                response.sendRedirect("/show_users.jsp");
            } else if (toAccess != null && toAccess && user.getRole_id()==2) {
                response.sendRedirect("/user_cabinet.jsp");
            }else {
                response.sendRedirect("/index.html");
            }
        }else {
            response.sendRedirect("/index.html");
        }
    }
}
