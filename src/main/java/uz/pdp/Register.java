package uz.pdp;

import lombok.SneakyThrows;
import uz.pdp.dbConnection.DBConnection;
import uz.pdp.model.Users;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;

public class Register extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/register.jsp");
        dispatcher.forward(request,response);
    }

    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Connection connection = DBConnection.createConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(
                "INSERT INTO users(user_name,password,phone_number,roles_id,is_active) values(?,?,?,?,?)");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String phoneNumber = request.getParameter("phoneNumber");
        preparedStatement.setString(1,username);
        preparedStatement.setString(2,password);
        preparedStatement.setString(3,phoneNumber);
        preparedStatement.setInt(4,2);
        preparedStatement.setBoolean(5,true);
        preparedStatement.execute();

        Users user=DBConnection.getUserByPassUserName(username,password);
        HttpSession session = request.getSession();
        session.setAttribute("user",user);
        response.sendRedirect("/user_cabinet.jsp");

    }
}

