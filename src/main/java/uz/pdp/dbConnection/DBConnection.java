package uz.pdp.dbConnection;
import uz.pdp.model.Messages;
import uz.pdp.model.Users;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class DBConnection {
    private static final String DB_URL="jdbc:postgresql://localhost:5432/tg_chat";
    private static final String DB_USERNAME="postgres";
    private static final String DB_PASSWORD="root123";

    public static Connection createConnection() throws ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        return DriverManager.getConnection(DB_URL,DB_USERNAME,DB_PASSWORD);
    }


    public static List<Users> getUsers(int role_id, Integer userId) throws SQLException, ClassNotFoundException {
        Connection connection = createConnection();
        String query;
        if (userId != null){
            query="select * from users where roles_id='"+role_id+"' and id != '"+userId+"';";
        }else {
            query="select * from users where roles_id='"+role_id+"';";
        }
        ResultSet resultSet = connection.createStatement().executeQuery(query);
        List<Users> usersList = new ArrayList<>();
        while (resultSet.next()){
            Users user = new Users();
            user.setId(resultSet.getInt(1));
            user.setRole_id(resultSet.getInt(2));
            user.setUserName(resultSet.getString(3));
            user.setPassword(resultSet.getString(4));
            user.setPhoneNumber(resultSet.getString(5));
            user.setActive(resultSet.getBoolean(6));

            usersList.add(user);
        }
        return usersList;
    }

//
//    public static Book getBookById(int parseInt) throws SQLException, ClassNotFoundException {
//        Connection connection = createConnection();
//        ResultSet resultSet = connection.createStatement().executeQuery("select * from book where id="+parseInt+";");
//        Book book =null;
//        while (resultSet.next()){
//            book=new Book();
//            book.setId(resultSet.getInt(1));
//            book.setName(resultSet.getString(2));
//        }
//        return book;
//    }
//
//    public static void saveOrEdit(String id, String name) throws SQLException, ClassNotFoundException {
//        String query="";
//        if (id != null){
//            query="update book set name='"+name+"' where id ="+Integer.parseInt(id)+";";
//        }else {
//            query="insert into book(name,action) values ('"+name+"','INSERT');";
//        }
//        Connection connection = createConnection();
//        Statement statement = connection.createStatement();
//        statement.execute(query);
//    }
//
    public static void setActiveOrBlock(Integer id, boolean setActiveOrBlock) throws SQLException, ClassNotFoundException {
        Connection connection = DBConnection.createConnection();
        Statement statement = connection.createStatement();
        statement.execute("update users set is_active="+setActiveOrBlock+" where id="+id+";");
    }
//
    public static Users getUserByPassUserName(String username, String password) throws SQLException, ClassNotFoundException {
        Connection connection = createConnection();
        PreparedStatement preparedStatement = connection.prepareStatement("select * from users where user_name='" + username + "' and password='" + password + "' ;");
        ResultSet resultSet = preparedStatement.executeQuery();

        Users user=new Users();
        while (resultSet.next()){
            user.setId(resultSet.getInt(1));
            user.setRole_id(resultSet.getInt(2));
            user.setUserName(resultSet.getString(3));
            user.setPassword(resultSet.getString(4));
            user.setPhoneNumber(resultSet.getString(5));
            user.setActive(resultSet.getBoolean(6));
        }
        return user;
    }
//
    public static List<Messages> getMessagesByUserId(boolean TrueIsRead_FalseIsUnRead,Integer userId) throws SQLException, ClassNotFoundException {
        Connection connection = createConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(
                "select * from messages where to_user='" + userId + "' and is_read="+TrueIsRead_FalseIsUnRead+" and is_deleted=false;");
        ResultSet resultSet = preparedStatement.executeQuery();

       List<Messages> messagesList=new ArrayList<>();
        while (resultSet.next()){
            Messages message=new Messages(
                    resultSet.getInt(1),
                    resultSet.getInt(2),
                    resultSet.getInt(3),
                    resultSet.getString(4),
                    resultSet.getDate(5),
                    resultSet.getBoolean(6),
                    resultSet.getBoolean(7)
            );
           messagesList.add(message);
        }
        return messagesList;
    }

    public static Users getUserById(Integer userId) throws SQLException, ClassNotFoundException {
        Connection connection = createConnection();
        PreparedStatement statement = connection.prepareStatement("select * from users where id='" + userId + "';");
        ResultSet resultSet = statement.executeQuery();
        Users user=new Users();
        while (resultSet.next()){
            user.setId(resultSet.getInt(1));
            user.setRole_id(resultSet.getInt(2));
            user.setUserName(resultSet.getString(3));
            user.setPassword(resultSet.getString(4));
            user.setPhoneNumber(resultSet.getString(5));
            user.setActive(resultSet.getBoolean(6));
        }
        return user;
    }

    public static void deleteMessage(Integer messageId) throws SQLException, ClassNotFoundException {
        Connection connection = createConnection();
        Statement statement = connection.createStatement();
        statement.execute("update messages set is_deleted=true where id="+messageId);
    }

    public static void setRead(Messages messages) throws SQLException, ClassNotFoundException {
        Connection connection = createConnection();
        Statement statement = connection.createStatement();
        statement.execute("update messages set is_read=true where id="+messages.getId()+";");
    }

    public static void saveMessage(Users toUser,Users fromUser,String text) throws SQLException, ClassNotFoundException {
        Connection connection = createConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(
                "insert into messages(from_user,to_user,text,date,is_read,is_deleted) " +
                "VALUES(?,?,?,now(),false,false);");
        preparedStatement.setInt(1,fromUser.getId());
        preparedStatement.setInt(2,toUser.getId());
        preparedStatement.setString(3,text);
        preparedStatement.execute();
    }
//
//    public static List<Book> getBookListByUserId(int userId) throws SQLException, ClassNotFoundException {
//        Connection connection = createConnection();
//        PreparedStatement preparedStatement = connection.prepareStatement(
//                "select id,name from book EXCEPT " +
//                        "select b.id,b.name from user_vs_book ub " +
//                "join users u on u.id=" + userId + " and u.id=ub.user_id" +
//                " join book b on b.id=ub.book_id");
//        ResultSet resultSet = preparedStatement.executeQuery();
//        List<Book> books=new ArrayList<>();
//        while (resultSet.next()){
//            int id = resultSet.getInt(1);
//            String name = resultSet.getString(2);
//            Book book=new Book(id,name);
//            books.add(book);
//        }
//        return books;
//    }
//    public static List<Book> getUnreadBookListByUserId(int userId) throws SQLException, ClassNotFoundException {
//        Connection connection = createConnection();
//        PreparedStatement preparedStatement = connection.prepareStatement(
//                "select b.id,b.name from user_vs_book ub " +
//                        "join users u on u.id=" + userId + " and u.id=ub.user_id" +
//                        " join book b on b.id=ub.book_id");
//        ResultSet resultSet = preparedStatement.executeQuery();
//        List<Book> books=new ArrayList<>();
//        while (resultSet.next()){
//            int id = resultSet.getInt(1);
//            String name = resultSet.getString(2);
//            Book book=new Book(id,name);
//            books.add(book);
//        }
//        return books;
//    }
}
