package uz.pdp;

import lombok.SneakyThrows;
import uz.pdp.dbConnection.DBConnection;
import uz.pdp.model.Messages;
import uz.pdp.model.Users;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class ShowMessages extends HttpServlet {
    @SneakyThrows
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String aNew = request.getParameter("new");
        String device = request.getParameter("device");
        HttpSession session = request.getSession();
        Users user = (Users) session.getAttribute("user");
        boolean yesNew=false;
        if (aNew != null){
            if (aNew.equals("1")){
                List<Messages> messagesList = DBConnection.getMessagesByUserId(false, user.getId());
                session.setAttribute("messages",messagesList);
                session.setAttribute("isNew",true);
                response.sendRedirect("/show_messages.jsp");
                yesNew=true;
            }else{
                List<Messages> messagesList = DBConnection.getMessagesByUserId(true, user.getId());
                session.setAttribute("messages",messagesList);
                session.setAttribute("isNew",false);
                response.sendRedirect("/show_messages.jsp");
                yesNew=true;
            }
        } else if (aNew == null && device == null){
            response.sendRedirect("/index.html");
        }
       if (!yesNew){
           if (device != null){
               String browserType = request.getHeader("User-Agent");
               PrintWriter writer = response.getWriter();
               writer.write(browserType);
           }else {
               response.sendRedirect("/index.html");
           }
       }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
