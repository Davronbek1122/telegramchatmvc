package uz.pdp;

import lombok.SneakyThrows;
import uz.pdp.dbConnection.DBConnection;
import uz.pdp.model.Users;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.List;

public class WriteMessage extends HttpServlet {
    @SneakyThrows
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String to_user_id = request.getParameter("to_user_id");
        String from_user_id = request.getParameter("from_user_id");
        if (from_user_id != null && to_user_id != null){
            HttpSession session = request.getSession();
            Users user = DBConnection.getUserById(Integer.valueOf(to_user_id));
            session.setAttribute("toUser",user);
            response.sendRedirect("/writeMessage.jsp");
        }else {
            HttpSession session = request.getSession();
            Users user = (Users) session.getAttribute("user");
            List<Users> usersList = DBConnection.getUsers(2, user.getId());
            session.setAttribute("users", usersList);
            response.sendRedirect("/show_users_for_write.jsp");
        }
    }

    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Users user = (Users) session.getAttribute("user");
        Users toUser = (Users) session.getAttribute("toUser");
        String text =request.getParameter("text");

        DBConnection.saveMessage(toUser,user,text);
        response.sendRedirect("/writeMessage.jsp");
    }
}
