package uz.pdp.utilss;

import uz.pdp.model.Users;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class Methods {

    public static Boolean checkToAccess(HttpServletRequest request, Integer roleId){
        HttpSession session = request.getSession();
        Users user = (Users) session.getAttribute("user");
        if (roleId != null && user != null){
            boolean equal=user.getRole_id().equals(roleId);
            if (equal){
                return true;
            } else if (!equal) {
                return false;
            }else return null;
        }
        return null;
    }
}
