package uz.pdp;

import lombok.SneakyThrows;
import uz.pdp.dbConnection.DBConnection;
import uz.pdp.model.Users;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ShowAllUsers extends HttpServlet {
    @SneakyThrows
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Users user = (Users) session.getAttribute("user");
        List<Users> usersList = new ArrayList<>();
        if (user.getRole_id() == 2){
            usersList= DBConnection.getUsers(2,user.getId());
        }else {
            usersList= DBConnection.getUsers(2, null);
        }
        session.setAttribute("users", usersList);
        session.setAttribute("user",user);
        response.sendRedirect("/show_users_for_write.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
