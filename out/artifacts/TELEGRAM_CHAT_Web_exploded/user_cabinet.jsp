<%@ page import="uz.pdp.model.Messages" %>
<%@ page import="uz.pdp.model.Users" %><%--
  Created by IntelliJ IDEA.
  User: hp
  Date: 18.09.2022
  Time: 13:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User Cabinet</title>
</head>
<body>
<br>
<%
    HttpSession session1 = request.getSession();
    Users current_user = (Users) session1.getAttribute("user");
    out.println("<center><h2 style=\"color:green\">Welcome to your cabinet " + current_user.getUserName() + "</h2></center>");
%>
<br>
<center><h3>
    <a href="/show_messages?new=1" style="text-decoration: none;">Show new Messages</a>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="/show_messages?new=0" style="text-decoration: none;">Show all my Message</a>
</h3></center>
<%--<center><h3><a href="/show_messages?new=0">Show all my Message</a></h3></center>--%>
<br>
<center><h3>
    <a href="/show_messages?device=0" style="text-decoration: none;">Show my active devices</a>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="/writeMessage" style="text-decoration: none;">Write new message</a>
</h3></center>
<br>
<center>
    <h3>
        <button >
            <a href="/index.html" style="text-decoration: none; color: red">LogOut</a>
        </button>
    </h3>
</center>
<%--<center><h3><a href="/writeMessage">Write new message</a></h3></center>--%>
</body>
</html>
